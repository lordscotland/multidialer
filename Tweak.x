#include "substrate.h"
#include "MDActionSheetDelegate.h"

NSString* SBSCopyLocalizedApplicationNameForDisplayIdentifier(NSString* appID);
void (*$$_CTCallDialWithID)(NSString* number,int abUID);
static void (*$$__CTCallDial)(NSString* number);
static void $__CTCallDial(NSString* number);

static NSDictionary* $_sharedConfig=nil;
BOOL $_passthru=NO;

@interface EXTERNAL
-(BOOL)hasTelephonyScheme;
-(id)bundleIdentifier;
@end

@interface UIApplication (Private)
-(id)displayIDForURLScheme:(id)scheme isPublic:(BOOL)public;
@end

void $__CTCallDial(NSString* number){
  if($_passthru){return $$__CTCallDial(number);}
  const char* pn=[number UTF8String];
  unsigned int len=strlen(pn),i,k;
  char* pnq=(char*)malloc(len+1);
  BOOL ifmt=1;
  for (i=k=0;i<len;i++){
    BOOL xdigit=strchr("*#",pn[i])!=NULL;if(xdigit){ifmt=0;}
    if(xdigit || strchr("0123456789",pn[i])!=NULL){pnq[k++]=pn[i];}
  }
  pnq[len=k]=0;
  UIActionSheet* sheet=[[UIActionSheet alloc] init];
  if(ifmt){
    char* pnf=(char*)malloc(len+4);
    for (i=k=0;i<len;i++){
      pnf[k++]=pnq[i];
      if(i==len-5 || i==len-8 || i==len-11){pnf[k++]='-';}
    }
    pnf[k]=0;
    sheet.title=[NSString stringWithUTF8String:pnf];
    free(pnf);
  }
  else {sheet.title=[NSString stringWithUTF8String:pnq];}
  UIApplication* application=[UIApplication sharedApplication];
  NSString* title=@"Open in \"%@\"";
  NSMutableArray* options=[NSMutableArray arrayWithCapacity:2];
  NSURL* URL;
  for (NSString* URLfmt in [$_sharedConfig objectForKey:@"URLs"]){
    if([application canOpenURL:URL=[NSURL URLWithString:
     [NSString stringWithFormat:URLfmt,pnq]]]){
      [options addObject:URL];
      [sheet addButtonWithTitle:[NSString stringWithFormat:
       NSLocalizedStringFromTableInBundle(title,nil,
       [NSBundle bundleWithIdentifier:@"com.apple.UIKit"],title),
       SBSCopyLocalizedApplicationNameForDisplayIdentifier(
       [application displayIDForURLScheme:[URL scheme] isPublic:YES])]];
    }
  }
  free(pnq);
  NSString* table=@"General";
  NSBundle* bundle=[NSBundle bundleWithIdentifier:@"com.apple.TelephonyUI"];
  if($$_CTCallDialWithID){
    [options addObject:number];
    [sheet addButtonWithTitle:NSLocalizedStringFromTableInBundle(@"CALL",table,bundle,@"CALL")];
  }
  sheet.cancelButtonIndex=[sheet addButtonWithTitle:NSLocalizedStringFromTableInBundle(@"CANCEL",table,bundle,@"CANCEL")];
  sheet.delegate=[[MDActionSheetDelegate alloc] initWithOptions:options];
  [sheet showInView:application.keyWindow];
  [sheet release];
}

%hook SpringBoard
-(void)applicationOpenURL:(id)URL publicURLsOnly:(BOOL)only animating:(BOOL)animating sender:(id)sender {
  $_passthru=[URL hasTelephonyScheme] && [[$_sharedConfig objectForKey:@"passthru"]
   containsObject:[sender bundleIdentifier]];
  %orig;
  $_passthru=NO;
}
%end

%hook SBAssistantController
-(void)assistantConnection:(id)connection didRecognizeSpeechPhrases:(id)phrases correctionIdentifier:(id)identifier {
  $_passthru=YES;
  %orig;
  $_passthru=NO;
}
%end

%hook SBVoiceControlMenuedAlertDisplay
-(id)_openTelURL:(id)URL {
  $_passthru=YES;
  id $_=%orig;
  $_passthru=NO;
  return $_;
}
%end

static inline void* getAddress(const char *file,const char* symbol){
  struct nlist nl[2];
  bzero(&nl,sizeof(nl));
  nl[0].n_un.n_name=(char*)symbol;
  if(nlist(file,nl)<0 || nl[0].n_type==N_UNDF){return NULL;}
  uintptr_t addr=nl[0].n_value;
  if(nl[0].n_desc&N_ARM_THUMB_DEF){addr|=1;}
  return (void*)addr;
}

%ctor {
  NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
  %init;
  const char* CoreTelephony="/System/Library/Frameworks/CoreTelephony.framework/CoreTelephony";
  MSHookFunction(getAddress(CoreTelephony,"__CTCallDial"),$__CTCallDial,(void*)&$$__CTCallDial);
  $$_CTCallDialWithID=getAddress(CoreTelephony,"_CTCallDialWithID");
  NSString* path=@"/Library/Preferences/com.officialscheduler.multidialer.plist";
  $_sharedConfig=[NSDictionary dictionaryWithContentsOfFile:
   [NSHomeDirectory() stringByAppendingPathComponent:path]];
  if(!$_sharedConfig){$_sharedConfig=[NSDictionary dictionaryWithContentsOfFile:path];}
  [$_sharedConfig retain];
  [pool drain];
}
