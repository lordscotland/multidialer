@interface EXTERNAL
-(void)showActionSheetForNumber:(id)number name:(id)name label:(id)label actions:(int)actions;
@end

%hook GoogleVoiceDialerAppDelegate
-(BOOL)application:(id)app handleOpenURL:(NSURL*)URL {
  BOOL ok=%orig;
  if(ok){[self showActionSheetForNumber:[URL host] name:nil label:nil actions:7];}
  return ok;
}
%end
